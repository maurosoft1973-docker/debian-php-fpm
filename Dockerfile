FROM maurosoft1973/debian

ARG BUILD_DATE
ARG DEBIAN_RELEASE
ARG DEBIAN_VERSION
ARG DEBIAN_VERSION_DATE
ARG PHP_VERSION
ARG PHP_VERSION_DATE

LABEL \
    maintainer="Mauro Cardillo <mauro.cardillo@gmail.com>" \
    architecture="amd64/x86_64" \
    php-version="$PHP_VERSION" \
    debian-version="$DEBIAN_RELEASE-$DEBIAN_VERSION" \
    build="$BUILD_DATE" \
    org.opencontainers.image.title="debian-php-fpm" \
    org.opencontainers.image.description="PHP-FPM Docker image running on Debian Linux" \
    org.opencontainers.image.authors="Mauro Cardillo <mauro.cardillo@gmail.com>" \
    org.opencontainers.image.vendor="Mauro Cardillo" \
    org.opencontainers.image.version="v$PHP_VERSION" \
    org.opencontainers.image.url="https://hub.docker.com/r/maurosoft1973/debian-php-fpm/" \
    org.opencontainers.image.source="https://github.com/maurosoft1973/debian-php-fpm" \
    org.opencontainers.image.created=$BUILD_DATE

RUN export DEBIAN_FRONTEND="noninteractive" && \
    wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg && \
    echo "deb https://packages.sury.org/php/ buster main" > /etc/apt/sources.list.d/php.list && \
    apt update && \
    apt upgrade -y && \
    mkdir -p /var/run/php && \
    apt install -y \
    locate \
    #php$PHP_VERSION-pear \
    php$PHP_VERSION-dev \
    php$PHP_VERSION-common \
    php$PHP_VERSION-cli \
    php$PHP_VERSION-ctype \
    php$PHP_VERSION-mysqlnd \
    php$PHP_VERSION-mysqli \
    php$PHP_VERSION-json \
    php$PHP_VERSION-curl \
    php$PHP_VERSION-opcache \
    php$PHP_VERSION-xml \
    php$PHP_VERSION-zip \
    php$PHP_VERSION-apcu \
    php$PHP_VERSION-gd \
    php$PHP_VERSION-iconv \
    php$PHP_VERSION-imagick \
    php$PHP_VERSION-mbstring \
    php$PHP_VERSION-mcrypt \
    php$PHP_VERSION-fileinfo \
    php$PHP_VERSION-posix \
    php$PHP_VERSION-imap \
    php$PHP_VERSION-ssh2 \
    #php$PHP_VERSION-session \
    #php$PHP_VERSION-openssl \
    php$PHP_VERSION-pdo \
    #php$PHP_VERSION-pdo_mysql \
    php$PHP_VERSION-fpm \
    php$PHP_VERSION-dom \
    php$PHP_VERSION-simplexml \
    php$PHP_VERSION-xmlwriter \
    php$PHP_VERSION-intl \
    php$PHP_VERSION-bcmath && \
    pecl install xdebug && \
    rm  -rf /etc/php/$PHP_VERSION/php/fpm/pool.d/www.conf && \
    apt purge -y php$PHP_VERSION-dev && \
    rm -rf /tmp/pear

ADD files/run-debian-php-fpm.sh /scripts/run-debian-php-fpm.sh

RUN chmod -R 755 /scripts

VOLUME ["/var/www"]

ENTRYPOINT ["/scripts/run-debian-php-fpm.sh"]
