#!/bin/bash

DEBUG=${DEBUG:-"0"}
PHP_VERSION=${PHP_VERSION:-"7.4"}
PORT=${PORT:-"17000"}
WWW_USER=${WWW_USER:-"www"}
WWW_USER_UID=${WWW_USER_UID:-"5001"}
WWW_GROUP=${WWW_GROUP:-"www-data"}
WWW_GROUP_UID=${WWW_GROUP_UID:-"33"}
PHP_POOL_PM_MODE=${PHP_POOL_PM_MODE:-"dynamic"}
PHP_POOL_PM_MAX_CHILDREN=${PHP_POOL_MAX_CHILDREN:-"5"}
PHP_POOL_PM_START_SERVERS=${PHP_POOL_START_SERVERS:-"2"}
PHP_POOL_PM_MIN_SPARE_SERVERS=${PHP_POOL_MIN_SPARE_SERVERS:-"1"}
PHP_POOL_PM_MAX_SPARE_SERVERS=${PHP_POOL_MAX_SPARE_SERVERS:-"3"}
PHP_POOL_REQUEST_TERMINATE_TIMEOUT=${PHP_POOL_REQUEST_TERMINATE_TIMEOUT:-"300"}
PHP_XDEBUG_ENABLED=${PHP_XDEBUG_ENABLED:-"1"}
PHP_XDEBUG_CLIENT_PORT=${PHP_XDEBUG_CLIENT_PORT:-"9000"}
PHP_XDEBUG_DISCOVER_CLIENT_HOST=${PHP_XDEBUG_DISCOVER_CLIENT_HOST:-"1"}
PHP_XDEBUG_START_WITH_REQUEST=${PHP_XDEBUG_START_WITH_REQUEST:-"yes"}
PHP_XDEBUG_LOG=${PHP_XDEBUG_LOG:-"/tmp/xdebug.log"}
PHP_XDEBUG_MODE=${PHP_XDEBUG_MODE:-"debug,develop"}

source /scripts/init-debian.sh

#Create Group (if not exist)
CHECK=$(cat /etc/group | grep $WWW_GROUP | wc -l)
if [ ${CHECK} == 0 ]; then
    echo "Create Group $WWW_GROUP with uid $WWW_GROUP_UID"
    groupadd -g ${WWW_GROUP_UID} ${WWW_GROUP}
else
    echo -e "Skipping,group $WWW_GROUP exist"
fi

#Create User (if not exist)
CHECK=$(cat /etc/passwd | grep $WWW_USER | wc -l)
if [ ${CHECK} == 0 ]; then
    echo "Create User $WWW_USER with uid $WWW_USER_UID"
    useradd -s /bin/false -M -u ${WWW_USER_UID} ${WWW_USER}
else
    echo -e "Skipping,user $WWW_USER exist"
fi

echo "Change Timezone ${TIMEZONE} php.ini file"
echo "date.timezone = '${TIMEZONE}'" >> /etc/php/${PHP_VERSION}/fpm/php.ini

/usr/bin/updatedb
XDEBUG_PATH=$(/usr/bin/locate xdebug.so)

if [ "$PHP_XDEBUG_ENABLED" == "1" ]; then
    echo "zend_extension=$XDEBUG_PATH" >> /etc/php/${PHP_VERSION}/fpm/php.ini
    echo "xdebug.client_port = ${PHP_XDEBUG_CLIENT_PORT}" >> /etc/php/${PHP_VERSION}/fpm/php.ini
    echo "xdebug.discover_client_host = ${PHP_XDEBUG_DISCOVER_CLIENT_HOST}" >> /etc/php/${PHP_VERSION}/fpm/php.ini
    echo "xdebug.start_with_request  = ${PHP_XDEBUG_START_WITH_REQUEST}" >> /etc/php/${PHP_VERSION}/fpm/php.ini
    echo "xdebug.log = ${PHP_XDEBUG_LOG}" >> /etc/php/${PHP_VERSION}/fpm/php.ini
    echo "xdebug.mode = ${PHP_XDEBUG_MODE}" >> /etc/php/${PHP_VERSION}/fpm/php.ini
fi

PHP_POOL_USER=/etc/php/${PHP_VERSION}/fpm/pool.d/$WWW_USER.conf

if [ -f "$PHP_POOL_USER" ]; then
    rm -rf $PHP_POOL_USER
fi

echo "Create configuration php for user $WWW_USER"
echo "[$WWW_USER]" >> $PHP_POOL_USER
echo "user = $WWW_USER"  >> $PHP_POOL_USER
echo "group = $WWW_GROUP"  >> $PHP_POOL_USER
echo "listen = $IP:$PORT" >> $PHP_POOL_USER
echo "pm = $PHP_POOL_PM_MODE" >> $PHP_POOL_USER
echo "pm.max_children = $PHP_POOL_PM_MAX_CHILDREN" >> $PHP_POOL_USER
echo "pm.start_servers = $PHP_POOL_PM_START_SERVERS" >> $PHP_POOL_USER
echo "pm.min_spare_servers = $PHP_POOL_PM_MIN_SPARE_SERVERS" >> $PHP_POOL_USER
echo "pm.max_spare_servers = $PHP_POOL_PM_MAX_SPARE_SERVERS" >> $PHP_POOL_USER
echo "pm.status_path = /status" >> $PHP_POOL_USER
echo "ping.path = /ping" >> $PHP_POOL_USER
echo "ping.response = pong" >> $PHP_POOL_USER
echo "request_terminate_timeout = $PHP_POOL_REQUEST_TERMINATE_TIMEOUT" >> $PHP_POOL_USER

echo "Listen on $IP:$PORT"

if [ "${DEBUG}" -eq "0" ]; then
    if [ "$WWW_USER" == "root" ]; then
        /usr/sbin/php-fpm${PHP_VERSION} --nodaemonize -R
    else 
        /usr/sbin/php-fpm${PHP_VERSION} --nodaemonize
    fi
else
    /bin/sh
fi
