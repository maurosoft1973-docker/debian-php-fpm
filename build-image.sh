#!/bin/bash
# Description: Build image and push to repository
# Maintainer: Mauro Cardillo
# DOCKER_HUB_USER and DOCKER_HUB_PASSWORD is user environment variable
echo "Get Remote Environment Variable"
wget -q "https://gitlab.com/maurosoft1973-docker/alpine-variable/-/raw/master/.env" -O ./.env
source ./.env

BUILD_DATE=$(date +"%Y-%m-%d")
IMAGE=maurosoft1973/debian-php-fpm
PHP_VERSION=7.4
PHP_VERSION_DATE="Mar 05, 2021"

# Loop through arguments and process them
for arg in "$@"
do
    case $arg in
       -ar=*|--debian-release=*)
        DEBIAN_RELEASE="${arg#*=}"
        shift # Remove
        ;;
        -av=*|--debian-version=*)
        DEBIAN_VERSION="${arg#*=}"
        shift # Remove
        ;;
        -avd=*|--debian-version-date=*)
        DEBIAN_VERSION_DATE="${arg#*=}"
        shift # Remove
        ;;
        -pv=*|--php-version=*)
        PHP_VERSION="${arg#*=}"
        shift # Remove
        ;;
        -pvd=*|--php-version-date=*)
        PHP_VERSION_DATE="${arg#*=}"
        shift # Remove
        ;;
        -r=*|--release=*)
        RELEASE="${arg#*=}"
        shift # Remove
        ;;
        -h|--help)
        echo -e "usage "
        echo -e "$0 "
        echo -e "  -ar=|--debian-release -> ${DEBIAN_RELEASE} (debian release)"
        echo -e "  -av=|--debian-version -> ${DEBIAN_VERSION} (debian version)"
        echo -e "  -avd=|--debian-version-date -> ${DEBIAN_VERSION_DATE} (debian version date)"
        echo -e "  -pv=|--php-version -> ${PHP_VERSION} (php version)"
        echo -e "  -pvd=|--php-version-date -> ${PHP_VERSION_DATE} (php version date)"
        echo -e "  -r=|--release -> ${RELEASE} (release of image.Values: TEST, CURRENT, LATEST)"
        exit 0
        ;;
    esac
done

echo "# Image               : ${IMAGE}"
echo "# Image Release       : ${RELEASE}"
echo "# Build Date          : ${BUILD_DATE}"
echo "# Debian Release      : ${DEBIAN_RELEASE}"
echo "# Debian Version      : ${DEBIAN_VERSION}"
echo "# Debian Version Date : ${DEBIAN_VERSION_DATE}"
echo "# PHP Version         : ${PHP_VERSION}"
echo "# PHP Version Date    : ${PHP_VERSION_DATE}"

if [ "$RELEASE" == "TEST" ]; then
    echo "Remove image ${IMAGE}:test"
    docker rmi -f ${IMAGE}:test > /dev/null 2>&1

    echo "Remove image ${IMAGE}:${PHP_VERSION}-test"
    docker rmi -f ${IMAGE}:${PHP_VERSION}-test > /dev/null 2>&1

    echo "Build Image: ${IMAGE} -> ${RELEASE}"
    docker build --build-arg BUILD_DATE=${BUILD_DATE} --build-arg DEBIAN_RELEASE=${DEBIAN_RELEASE} --build-arg DEBIAN_VERSION=${DEBIAN_VERSION} --build-arg DEBIAN_VERSION_DATE="${DEBIAN_VERSION_DATE}" --build-arg PHP_VERSION=${PHP_VERSION} --build-arg PHP_VERSION_DATE="${PHP_VERSION_DATE}" -t ${IMAGE}:test -t ${IMAGE}:${PHP_VERSION}-test -f ./Dockerfile .

    echo "Login Docker HUB"
    echo "$DOCKER_HUB_PASSWORD" | docker login -u "$DOCKER_HUB_USER" --password-stdin

    echo "Push Image -> ${IMAGE}:${PHP_VERSION}-test"
    docker push ${IMAGE}:${PHP_VERSION}-test

    echo "Push Image -> ${IMAGE}:test"
    docker push ${IMAGE}:test
elif [ "$RELEASE" == "CURRENT" ]; then
    echo "Remove image ${IMAGE}:${PHP_VERSION}"
    docker rmi -f ${IMAGE}:${PHP_VERSION} > /dev/null 2>&1

    echo "Remove image ${IMAGE}:${PHP_VERSION}-amd64"
    docker rmi -f ${IMAGE}:${PHP_VERSION}-amd64 > /dev/null 2>&1

    echo "Remove image ${IMAGE}:${PHP_VERSION}-x86_64"
    docker rmi -f ${IMAGE}:${PHP_VERSION}-x86_64 > /dev/null 2>&1

    echo "Build Image: ${IMAGE}:${PHP_VERSION} -> ${RELEASE}"
    docker build --build-arg BUILD_DATE=${BUILD_DATE} --build-arg DEBIAN_RELEASE=${DEBIAN_RELEASE} --build-arg DEBIAN_VERSION=${DEBIAN_VERSION} --build-arg DEBIAN_VERSION_DATE="${DEBIAN_VERSION_DATE}" --build-arg PHP_VERSION=${PHP_VERSION} --build-arg PHP_VERSION_DATE="${PHP_VERSION_DATE}" -t ${IMAGE}:${PHP_VERSION} -t ${IMAGE}:${PHP_VERSION}-amd64 -t ${IMAGE}:${PHP_VERSION}-x86_64 -f ./Dockerfile .

    echo "Login Docker HUB"
    echo "$DOCKER_HUB_PASSWORD" | docker login -u "$DOCKER_HUB_USER" --password-stdin

    echo "Push Image -> ${IMAGE}:${PHP_VERSION}-amd64"
    docker push ${IMAGE}:${PHP_VERSION}-amd64

    echo "Push Image -> ${IMAGE}:${PHP_VERSION}-x86_64"
    docker push ${IMAGE}:${PHP_VERSION}-x86_64

    echo "Push Image -> ${IMAGE}:${PHP_VERSION}"
    docker push ${IMAGE}:${PHP_VERSION}
else
    echo "Remove image ${IMAGE}:latest"
    docker rmi -f ${IMAGE} > /dev/null 2>&1

    echo "Remove image ${IMAGE}:amd64"
    docker rmi -f ${IMAGE}:amd64 > /dev/null 2>&1

    echo "Remove image ${IMAGE}:x86_64"
    docker rmi -f ${IMAGE}:x86_64 > /dev/null 2>&1

    echo "Build Image: ${IMAGE} -> ${RELEASE}"
    docker build --build-arg BUILD_DATE=${BUILD_DATE} --build-arg DEBIAN_RELEASE=${DEBIAN_RELEASE} --build-arg DEBIAN_VERSION=${DEBIAN_VERSION} --build-arg DEBIAN_VERSION_DATE="${DEBIAN_VERSION_DATE}" --build-arg PHP_VERSION=${PHP_VERSION} --build-arg PHP_VERSION_DATE="${PHP_VERSION_DATE}" -t ${IMAGE}:latest -t ${IMAGE}:amd64 -t ${IMAGE}:x86_64 -f ./Dockerfile .

    echo "Login Docker HUB"
    echo "$DOCKER_HUB_PASSWORD" | docker login -u "$DOCKER_HUB_USER" --password-stdin

    echo "Push Image -> ${IMAGE}:amd64"
    docker push ${IMAGE}:amd64

    echo "Push Image -> ${IMAGE}:x86_64"
    docker push ${IMAGE}:x86_64

    echo "Push Image -> ${IMAGE}:latest"
    docker push ${IMAGE}:latest
fi

rm -rf ./.env
rm -rf ./settings.sh
